## Installer les dépendances

```
pnpm install
```

### Si vous n'avez pas pnpm

```
npm install -g pnpm

pnpm install
```

## Lancer les tests

```
pnpm run test
```
