import type { JestConfigWithTsJest } from 'ts-jest'

const jestConfig: JestConfigWithTsJest = {
  rootDir: './test',
  testRegex: '.*\\.test\\.ts?$',
  transform: {
    '^.+\\.ts?$': 'ts-jest',
  },
}

export default jestConfig;
