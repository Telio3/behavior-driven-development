import { randomUUID } from "crypto";
import { Personne } from "./personne";
import { IPlageHoraire } from "../interfaces/plage-horaire.interface";
import { PLAGES_HORAIRES_TOUTE_LA_SEMAINE } from "../tools/plage-horaire.const";

export class Badge {
    public readonly id: string = randomUUID();
    public proprietaire: Personne | null = null;
    private _bloque: boolean = false;
    private _plagesHoraires: IPlageHoraire[] = []; // en UTC

    public get bloque(): boolean {
        return this._bloque;
    }

    constructor(plagesHoraires: IPlageHoraire[] = PLAGES_HORAIRES_TOUTE_LA_SEMAINE) {
        this._plagesHoraires = plagesHoraires;
    }

    public bloquer(): void {
        this._bloque = true;
    }

    public debloquer(): void {
        this._bloque = false;
    }

    public attribuerA(personne: Personne): void {
        this.proprietaire = personne;
    }

    public desattribuer(): void {
        this.proprietaire = null;
    }

    public estValide(): boolean {
        const date = new Date(Date.now());
        const jourCourant = date.getUTCDay();
        const heureCourante = date.getUTCHours();
        const minuteCourante = date.getUTCMinutes();
    
        const plagesValides = this._plagesHoraires.filter((plage) =>
            plage.jour === jourCourant &&
            plage.heureDebut <= heureCourante &&
            heureCourante < plage.heureFin &&
            (heureCourante !== plage.heureFin || minuteCourante < plage.minuteFin)
        );
    
        return plagesValides.length > 0;
    }
}
