import { IMoteurOuvertureLogger } from "../interfaces/moteur-ouverture-logger.interface";
import { ILecteur } from "../interfaces/lecteur.interface";
import { MoteurOuvertureLogs } from "../logs/moteur-ouverture.logs";
import { Badge } from "./badge";

export class MoteurOuverture {
    public badges: Array<Badge> = [];

    constructor(private readonly _lecteur: ILecteur, private readonly _logger?: IMoteurOuvertureLogger) {}

    public interrogerLecteurs(): void {
        try {
            const badge = this._lecteur.badgeNonBloqueDetecte();

            if (badge && this.badges.find((b) => b.id === badge.id)) {
                this._lecteur.porte.ouvrir();

                this._logger?.logOuverturePorte({
                    date: new Date(Date.now()),
                    proprietaire: badge.proprietaire!,
                    message: MoteurOuvertureLogs.PORTE_OUVERTE,
                });
            } else {
                this._logger?.logOuverturePorte({
                    date: new Date(Date.now()),
                    message: MoteurOuvertureLogs.BADGE_NON_RECONNU,
                });
            }
        } catch (error) {
            this._logger?.logException();
        }
    }

    public enregistrerBadge(badge: Badge): void {
        this.badges.push(badge);
    }
}
