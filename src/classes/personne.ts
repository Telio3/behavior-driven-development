import { ErreursPersonne } from "../erreurs/personne.erreurs";

export class Personne {
    public prenom!: string;
    public nom!: string;

    constructor(prenom: string, nom: string) {
        this._construirePrenom(prenom);
        this._construireNom(nom);
    }

    private _construirePrenom(prenom: string): void {
        // Vérifier la longueur minimale du prénom
        if (!this._tailleMinimum(prenom, 2)) {
            throw new Error(ErreursPersonne.LONGUEUR_MIN_PRENOM);
        }

        // Vérifier que le prénom ne contient que des lettres
        if (!this._seulementDesLettres(prenom)) {
            throw new Error(ErreursPersonne.UNIQUEMENT_LETTRES_PRENOM);
        }

        this.prenom = prenom;
    }

    private _construireNom(nom: string): void {
        // Vérification de la longueur minimale du nom de famille
        if (!this._tailleMinimum(nom, 1)) {
            throw new Error(ErreursPersonne.LONGUEUR_MIN_NOM);
        }

        // Vérifier que le nom de famille ne contient que des lettres
        if (!this._seulementDesLettres(nom)) {
            throw new Error(ErreursPersonne.UNIQUEMENT_LETTRES_NOM);
        }

        this.nom = nom;
    }

    private _seulementDesLettres(valeur: string): boolean {
        return /^[a-zA-Z]+$/.test(valeur);
    }

    private _tailleMinimum(valeur: string, taille: number): boolean {
        return valeur.length >= taille;
    }
}
