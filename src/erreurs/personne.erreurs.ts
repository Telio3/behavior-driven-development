export class ErreursPersonne {
    public static readonly LONGUEUR_MIN_PRENOM = "Le prénom doit comporter au moins 2 lettres.";
    public static readonly UNIQUEMENT_LETTRES_PRENOM = "Le prénom ne doit contenir que des lettres.";
    public static readonly LONGUEUR_MIN_NOM = "Le nom doit comporter au moins 1 lettre.";
    public static readonly UNIQUEMENT_LETTRES_NOM = "Le nom ne doit contenir que des lettres.";
}
