import { Badge } from "../classes/badge";
import { IPorte } from "./porte.interface";

export interface ILecteur {
    porte: IPorte;
    badgeNonBloqueDetecte: () => Badge | undefined;
}