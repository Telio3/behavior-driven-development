import { Personne } from "../classes/personne";
import { MoteurOuvertureLogs } from "../logs/moteur-ouverture.logs";

export interface ILogOuverturePorteParams {
    date: Date;
    proprietaire?: Personne;
    message: MoteurOuvertureLogs;

}

export interface IMoteurOuvertureLogger {
    logException(): void;
    logOuverturePorte(params: ILogOuverturePorteParams): void;
}