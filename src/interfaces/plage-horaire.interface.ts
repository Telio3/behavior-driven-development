export interface IPlageHoraire {
    jour: number; // Jour de la semaine (0 pour dimanche, 1 pour lundi, ..., 6 pour samedi)
    heureDebut: number; // Heure de début (0-23)
    minuteDebut: number; // Minute de début (0-59)
    heureFin: number; // Heure de fin (0-23)
    minuteFin: number; // Minute de fin (0-59)
};
