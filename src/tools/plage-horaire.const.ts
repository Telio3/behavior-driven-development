import { IPlageHoraire } from "../interfaces/plage-horaire.interface";

export const PLAGES_HORAIRES_TOUTE_LA_SEMAINE: IPlageHoraire[] = [
    { jour: 0, heureDebut: 0, minuteDebut: 0, heureFin: 23, minuteFin: 59 }, // Dimanche
    { jour: 1, heureDebut: 0, minuteDebut: 0, heureFin: 23, minuteFin: 59 }, // Lundi
    { jour: 2, heureDebut: 0, minuteDebut: 0, heureFin: 23, minuteFin: 59 }, // Mardi
    { jour: 3, heureDebut: 0, minuteDebut: 0, heureFin: 23, minuteFin: 59 }, // Mercredi
    { jour: 4, heureDebut: 0, minuteDebut: 0, heureFin: 23, minuteFin: 59 }, // Jeudi
    { jour: 5, heureDebut: 0, minuteDebut: 0, heureFin: 23, minuteFin: 59 }, // Vendredi
    { jour: 6, heureDebut: 0, minuteDebut: 0, heureFin: 23, minuteFin: 59 }, // Samedi
];
