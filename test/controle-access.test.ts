import { PorteSpy } from "./spies/porte.spy";
import { LoggerSpy } from "./spies/logger.spy";
import { Personne } from "../src/classes/personne";
import { MoteurOuvertureLogs } from "../src/logs/moteur-ouverture.logs";
import { MoteurOuvertureBuilder } from "./utilities/moteur-ouverture.builder";
import { BadgeBuilder } from "./utilities/badge.builder";

describe('Contrôle d\'accès', () => {
    test('Cas nominal', () => {
        // ÉTANT DONNÉ un Lecteur relié à une Porte
        // ET un Moteur d'Ouverture interrogeant ce Lecteur
        // ET un Badge valide appartenant à une Personne enregistrée dans le Moteur d'Ouverture
        const porte = new PorteSpy();
        const badge = new BadgeBuilder().avecProprietaire().build();
        const moteurOuverture = new MoteurOuvertureBuilder()
            .avecPorte(porte)
            .enregistrerBadges(badge)
            .simulerDetectionBadge(badge)
            .build();
        
        // QUAND un Badge valide est passé devant le Lecteur
        moteurOuverture.interrogerLecteurs();
        
        // ALORS un signal d'ouverture est envoyé à la Porte
        expect(porte.ouvertureAppelee).toBe(true);
    });

    test('Cas où aucun badge n\'est détecté', () => {
        // ÉTANT DONNÉ un Lecteur relié à une Porte
        // ET un Moteur d'Ouverture interrogeant ce Lecteur
        const porte = new PorteSpy();
        const moteurOuverture = new MoteurOuvertureBuilder()
            .avecPorte(porte)
            .build();
        
        // QUAND aucun Badge valide n'est passé devant le Lecteur
        moteurOuverture.interrogerLecteurs();
        
        // ALORS aucun signal d'ouverture n'est envoyé à la Porte
        expect(porte.ouvertureAppelee).toBe(false); 
    });

    test('Cas avec un lecteur défectueux', () => {
        // ÉTANT DONNÉ un Lecteur défectueux relié à une Porte
        const porte = new PorteSpy();
        const loggerSpy = new LoggerSpy();
        const moteurOuverture = new MoteurOuvertureBuilder()
            .avecPorte(porte)
            .avecLecteurCasse()
            .avecLogger(loggerSpy)
            .build();

        // QUAND on interroge ce Lecteur
        moteurOuverture.interrogerLecteurs();

        // ALORS aucun signal d'ouverture n'est envoyé à la Porte
        expect(porte.ouvertureAppelee).toBe(false);

        // ET l'erreur est loguée
        expect(loggerSpy.erreurAppelee).toBe(true);
    });

    test('Cas avec un badge bloqué', () => {
        // ÉTANT DONNÉ une Porte reliée à un Lecteur
        // ET un Badge appartenant à une Personne ayant été bloqué, enregistré dans le Moteur d'Ouverture
        const porte = new PorteSpy();
        const badge = new BadgeBuilder().avecProprietaire().bloque().build();
        const moteurOuverture = new MoteurOuvertureBuilder()
            .avecPorte(porte)
            .enregistrerBadges(badge)
            .simulerDetectionBadge(badge)
            .build();

        // QUAND ce Badge bloqué est passé devant le Lecteur
        moteurOuverture.interrogerLecteurs();

        // ALORS aucun signal d'ouverture n'est envoyé à la Porte
        expect(porte.ouvertureAppelee).toBe(false);
    });

    test('Cas avec un badge bloqué non attribué', () => {
        // ÉTANT DONNÉ une Porte reliée à un Lecteur
        // ET un Badge qui n'appartient pas à une Personne ayant été bloqué, enregistré dans le Moteur d'Ouverture
        const porte = new PorteSpy();
        const badge = new BadgeBuilder().bloque().build();
        const moteurOuverture = new MoteurOuvertureBuilder()
            .avecPorte(porte)
            .enregistrerBadges(badge)
            .simulerDetectionBadge(badge)
            .build();

        // QUAND ce Badge bloqué est passé devant le Lecteur
        moteurOuverture.interrogerLecteurs();

        // ALORS aucun signal d'ouverture n'est envoyé à la Porte
        expect(porte.ouvertureAppelee).toBe(false);
    });

    test('Cas avec un badge bloqué puis débloqué', () => {
        // ÉTANT DONNÉ une Porte reliée à un Lecteur
        // ET un Badge appartenant à une Personne ayant été bloqué puis débloqué, enregistré dans le Moteur d'Ouverture
        const porte = new PorteSpy();

        const badge = new BadgeBuilder().avecProprietaire().bloque().build();
        badge.debloquer();

        const moteurOuverture = new MoteurOuvertureBuilder()
            .avecPorte(porte)
            .enregistrerBadges(badge)
            .simulerDetectionBadge(badge)
            .build();

        // QUAND ce Badge bloqué puis débloqué est passé devant le Lecteur
        moteurOuverture.interrogerLecteurs();
        
        // ALORS un signal d'ouverture est envoyé à la Porte
        expect(porte.ouvertureAppelee).toBe(true);
    });

    test('Cas avec un badge attribué et enregistré', () => {
        // ÉTANT DONNÉ une Porte reliée à un Lecteur
        // ET un Badge appartenant à une Personne enregistrée dans le Moteur d'Ouverture
        const porte = new PorteSpy();
        const badge = new BadgeBuilder().avecProprietaire().build();
        const moteurOuverture = new MoteurOuvertureBuilder()
            .avecPorte(porte)
            .enregistrerBadges(badge)
            .simulerDetectionBadge(badge)
            .build();

        // QUAND ce Badge est passé devant le Lecteur
        moteurOuverture.interrogerLecteurs();

        // ALORS un signal d'ouverture est envoyé à la Porte
        expect(porte.ouvertureAppelee).toBe(true);
    });

    test('Cas avec un badge attribué à une personne puis réattribué à une autre personne', () => {
        // ETANT DONNE une Porte reliée à un Lecteur
        // Et un Badge qui appartient à une Personne enregistrée dans le Moteur d'Ouverture
        const loggerSpy = new LoggerSpy();
        const porte = new PorteSpy();

        const badge = new BadgeBuilder().avecProprietaire().build();
        const autrePersonne = new Personne('Jane', 'Doe');
        badge.attribuerA(autrePersonne);

        const moteurOuverture = new MoteurOuvertureBuilder()
            .avecPorte(porte)
            .avecLogger(loggerSpy)
            .enregistrerBadges(badge)
            .simulerDetectionBadge(badge)
            .build();

        // QUAND ce Badge est passé devant le Lecteur
        moteurOuverture.interrogerLecteurs();

        // ALORS un signal d'ouverture est envoyé à la Porte
        // ET un message est logué
        expect(porte.ouvertureAppelee).toBe(true);
        expect(loggerSpy.dernierLog().message).toBe(MoteurOuvertureLogs.PORTE_OUVERTE);
        expect(loggerSpy.dernierLog().proprietaire).toBe(autrePersonne);
    });

    test('Cas avec un badge attribué à une personne puis désattribué', () => {
        // ÉTANT DONNÉ une Porte reliée à un Lecteur
        // ET un Badge appartenant à une Personne enregistrée dans le Moteur d'Ouverture
        const loggerSpy = new LoggerSpy();
        const porte = new PorteSpy();

        const badge = new BadgeBuilder().avecProprietaire().build();
        badge.desattribuer();

        const moteurOuverture = new MoteurOuvertureBuilder()
            .avecPorte(porte)
            .avecLogger(loggerSpy)
            .enregistrerBadges(badge)
            .simulerDetectionBadge(badge)
            .build();

        // QUAND ce Badge est passé devant le Lecteur
        moteurOuverture.interrogerLecteurs();

        // ALORS aucun signal d'ouverture n'est envoyé à la Porte
        // ET un message n'est pas logué
        expect(porte.ouvertureAppelee).toBe(false);
        expect(loggerSpy.dernierLog().message).toBe(MoteurOuvertureLogs.BADGE_NON_RECONNU);
    });

    test('Cas avec un badge non attribué', () => {
        // ÉTANT DONNÉ une Porte reliée à un Lecteur
        // ET un Badge n'appartenant à aucune Personne enregistrée dans le Moteur d'Ouverture
        const loggerSpy = new LoggerSpy();
        const porte = new PorteSpy();
        const badge = new BadgeBuilder().build();
        const moteurOuverture = new MoteurOuvertureBuilder()
            .avecPorte(porte)
            .avecLogger(loggerSpy)
            .enregistrerBadges(badge)
            .simulerDetectionBadge(badge)
            .build();
        
        // QUAND ce Badge est passé devant le Lecteur
        moteurOuverture.interrogerLecteurs();

        // ALORS aucun signal d'ouverture n'est envoyé à la Porte
        // ET un message n'est pas logué
        expect(porte.ouvertureAppelee).toBe(false);
        expect(loggerSpy.dernierLog().message).toBe(MoteurOuvertureLogs.BADGE_NON_RECONNU);
    });

    test('Cas avec un badge attribué mais non enregistré', () => {
        // ÉTANT DONNÉ une Porte reliée à un Lecteur
        // ET un Badge appartenant à une Personne non enregistrée dans le Moteur d'Ouverture
        const porte = new PorteSpy();
        const badge = new BadgeBuilder().avecProprietaire().build();
        const moteurOuverture = new MoteurOuvertureBuilder()
            .avecPorte(porte)
            .simulerDetectionBadge(badge)
            .build();
        
        // QUAND ce Badge est passé devant le Lecteur
        moteurOuverture.interrogerLecteurs();

        // ALORS aucun signal d'ouverture n'est envoyé à la Porte
        expect(porte.ouvertureAppelee).toBe(false);
    });

    test('Cas avec un badge ayant des plages horaires valides', () => {
        // ÉTANT DONNÉ une Porte reliée à un Lecteur
        // ET un Badge avec des jours de validité qui correspondent à la date actuelle, une Personne rattachée et enregistrée dans le Moteur d'Ouverture

        jest.spyOn(global.Date, 'now').mockImplementation(() => new Date('2024-01-01T15:30:00Z').valueOf()); // Lundi 15h30

        const porte = new PorteSpy();
        const badge = new BadgeBuilder()
            .avecProprietaire()
            .avecPlagesHoraires([
                { jour: 1, heureDebut: 15, minuteDebut: 0, heureFin: 16, minuteFin: 0 } // Lundi de 15h à 16h
            ])
            .build();
        
        const moteurOuverture = new MoteurOuvertureBuilder()
            .avecPorte(porte)
            .enregistrerBadges(badge)
            .simulerDetectionBadge(badge)
            .build();

        // QUAND ce Badge est passé devant le Lecteur
        moteurOuverture.interrogerLecteurs();

        // ALORS un signal d'ouverture est envoyé à la Porte
        expect(porte.ouvertureAppelee).toBe(true);

        jest.spyOn(global.Date, 'now').mockRestore();
    });

    test('Cas avec un badge ayant des plages horaires invalides (jour invalide mais heure valide)', () => {
        // ÉTANT DONNÉ une Porte reliée à un Lecteur
        // ET un Badge avec des jours de validité qui ne correspondent pas à la date actuelle, une Personne rattachée et enregistrée dans le Moteur d'Ouverture

        jest.spyOn(global.Date, 'now').mockImplementation(() => new Date('2024-01-02T15:30:00Z').valueOf()); // Mardi 15h30

        const porte = new PorteSpy();
        const badge = new BadgeBuilder()
            .avecProprietaire()
            .avecPlagesHoraires([
                { jour: 1, heureDebut: 15, minuteDebut: 0, heureFin: 16, minuteFin: 0 } // Lundi de 15h à 16h
            ])
            .build();
        
        const moteurOuverture = new MoteurOuvertureBuilder()
            .avecPorte(porte)
            .enregistrerBadges(badge)
            .simulerDetectionBadge(badge)
            .build();

        // QUAND ce Badge est passé devant le Lecteur
        moteurOuverture.interrogerLecteurs();

        // ALORS aucun signal d'ouverture n'est envoyé à la Porte
        expect(porte.ouvertureAppelee).toBe(false);

        jest.spyOn(global.Date, 'now').mockRestore();
    });

    test('Cas avec un badge ayant des plages horaires invalides (jour valide mais heure invalide)', () => {
        // ÉTANT DONNÉ une Porte reliée à un Lecteur
        // ET un Badge avec des heures de validité qui ne correspondent pas à la date actuelle, une Personne rattachée et enregistrée dans le Moteur d'Ouverture

        jest.spyOn(global.Date, 'now').mockImplementation(() => new Date('2024-01-01T16:30:00Z').valueOf()); // Lundi 16h30

        const porte = new PorteSpy();
        const badge = new BadgeBuilder()
            .avecProprietaire()
            .avecPlagesHoraires([
                { jour: 1, heureDebut: 15, minuteDebut: 0, heureFin: 16, minuteFin: 0 } // Lundi de 15h à 16h
            ])
            .build();
        
        const moteurOuverture = new MoteurOuvertureBuilder()
            .avecPorte(porte)
            .enregistrerBadges(badge)
            .simulerDetectionBadge(badge)
            .build();

        // QUAND ce Badge est passé devant le Lecteur
        moteurOuverture.interrogerLecteurs();

        // ALORS aucun signal d'ouverture n'est envoyé à la Porte
        expect(porte.ouvertureAppelee).toBe(false);

        jest.spyOn(global.Date, 'now').mockRestore();
    });

    test('Cas avec un badge ayant des plages horaires invalides (jour et heure valides mais minute invalide)', () => {
        // ÉTANT DONNÉ une Porte reliée à un Lecteur
        // ET un Badge avec des minutes de validité qui ne correspondent pas à la date actuelle, une Personne rattachée et enregistrée dans le Moteur d'Ouverture

        jest.spyOn(global.Date, 'now').mockImplementation(() => new Date('2024-01-01T15:45:00Z').valueOf()); // Lundi 15h45

        const porte = new PorteSpy();
        const badge = new BadgeBuilder()
            .avecProprietaire()
            .avecPlagesHoraires([
                { jour: 1, heureDebut: 15, minuteDebut: 0, heureFin: 15, minuteFin: 30 } // Lundi de 15h à 15h30
            ])
            .build();
        
        const moteurOuverture = new MoteurOuvertureBuilder()
            .avecPorte(porte)
            .enregistrerBadges(badge)
            .simulerDetectionBadge(badge)
            .build();

        // QUAND ce Badge est passé devant le Lecteur
        moteurOuverture.interrogerLecteurs();

        // ALORS aucun signal d'ouverture n'est envoyé à la Porte
        expect(porte.ouvertureAppelee).toBe(false);

        jest.spyOn(global.Date, 'now').mockRestore();
    });

    test('Cas avec un badge ayant des plages horaires valides avec un fuseau horaire différent', () => {
        // ÉTANT DONNÉ une Porte reliée à un Lecteur
        // ET un Badge avec des plages horaires qui correspondent à la date actuelle avec un fuseau horaire différent, une Personne rattachée et enregistrée dans le Moteur d'Ouverture

        jest.spyOn(global.Date, 'now').mockImplementation(() => new Date('2024-01-01T15:30:00+01:00').valueOf()); // Lundi 15h30 au fuseau horaire +01:00

        const porte = new PorteSpy();
        const badge = new BadgeBuilder()
            .avecProprietaire()
            .avecPlagesHoraires([
                { jour: 1, heureDebut: 14, minuteDebut: 0, heureFin: 15, minuteFin: 0 } // Lundi de 14h à 15h au fuseau horaire +00:00
            ])
            .build();
        
        const moteurOuverture = new MoteurOuvertureBuilder()
            .avecPorte(porte)
            .enregistrerBadges(badge)
            .simulerDetectionBadge(badge)
            .build();

        // QUAND ce Badge est passé devant le Lecteur
        moteurOuverture.interrogerLecteurs();

        // ALORS un signal d'ouverture est envoyé à la Porte
        expect(porte.ouvertureAppelee).toBe(true);

        jest.spyOn(global.Date, 'now').mockRestore();
    });

    test('Cas avec un badge ayant des plages horaires invalides avec un fuseau horaire différent', () => {
        // ÉTANT DONNÉ une Porte reliée à un Lecteur
        // ET un Badge avec des plages horaires qui ne correspondent pas à la date actuelle avec un fuseau horaire différent, une Personne rattachée et enregistrée dans le Moteur d'Ouverture

        jest.spyOn(global.Date, 'now').mockImplementation(() => new Date('2024-01-01T15:30:00+01:00').valueOf()); // Lundi 15h30 au fuseau horaire +01:00

        const porte = new PorteSpy();
        const badge = new BadgeBuilder()
            .avecProprietaire()
            .avecPlagesHoraires([
                { jour: 1, heureDebut: 15, minuteDebut: 0, heureFin: 16, minuteFin: 0 } // Lundi de 15h à 16h au fuseau horaire +00:00
            ])
            .build();
        
        const moteurOuverture = new MoteurOuvertureBuilder()
            .avecPorte(porte)
            .enregistrerBadges(badge)
            .simulerDetectionBadge(badge)
            .build();

        // QUAND ce Badge est passé devant le Lecteur
        moteurOuverture.interrogerLecteurs();

        // ALORS aucun signal d'ouverture n'est envoyé à la Porte
        expect(porte.ouvertureAppelee).toBe(false);

        jest.spyOn(global.Date, 'now').mockRestore();
    });
});
