import { Badge } from "../../src/classes/badge";
import { IPorte } from "../../src/interfaces/porte.interface";
import { ILecteur } from "../../src/interfaces/lecteur.interface";

export class LecteurDummy implements ILecteur {
    public get porte(): IPorte {
        throw new Error("Le lecteur dummy n'est pas censé être appelé");
    }

    constructor(private _porte: IPorte) {}

    public simulerDetectionBadge(): void {
        throw new Error("Le lecteur dummy n'est pas censé être appelé");
    }

    public badgeNonBloqueDetecte(): Badge | undefined {
        throw new Error("Le lecteur dummy n'est pas censé être appelé");
    }
}
