import { Badge } from "../../src/classes/badge";
import { IPorte } from "../../src/interfaces/porte.interface";
import { ILecteur } from "../../src/interfaces/lecteur.interface";

export class LecteurFake implements ILecteur {
    private _prochainBadgeDetecte: Badge | undefined;

    constructor(public porte: IPorte) {}

    public simulerDetectionBadge(badge: Badge): void {
        this._prochainBadgeDetecte = badge;
    }

    public badgeNonBloqueDetecte(): Badge | undefined {
        if (this._prochainBadgeDetecte) {
            const badge = this._prochainBadgeDetecte;

            this._prochainBadgeDetecte = undefined;

            if (!badge.proprietaire || badge.bloque || !badge.estValide()) {
                return undefined;
            }

            return badge;
        }
        
        return undefined;
    }
}
