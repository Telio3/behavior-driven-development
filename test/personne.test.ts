import { Personne } from "../src/classes/personne";
import { ErreursPersonne } from "../src/erreurs/personne.erreurs";

describe('Création d\'une personne', () => {
    test('Création d\'une personne avec un prénom de moins de 2 lettres', () => {
        expect(() => {
            new Personne('J', 'Doe');
        }).toThrow(ErreursPersonne.LONGUEUR_MIN_PRENOM);
    });
    
    test('Création d\'une personne avec un prénom contenant des caractères non alphabétiques', () => {
        expect(() => {
            new Personne('John1', 'Doe');
        }).toThrow(ErreursPersonne.UNIQUEMENT_LETTRES_PRENOM);
    });
    
    test('Création d\'une personne avec un nom de moins de 1 lettre', () => {
        expect(() => {
            new Personne('John', '');
        }).toThrow(ErreursPersonne.LONGUEUR_MIN_NOM);
    });
    
    test('Création d\'une personne avec un nom contenant des caractères non alphabétiques', () => {
        expect(() => {
            new Personne('John', 'Doe1');
        }).toThrow(ErreursPersonne.UNIQUEMENT_LETTRES_NOM);
    });
});
