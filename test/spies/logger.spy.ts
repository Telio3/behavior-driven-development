import { ILogOuverturePorteParams, IMoteurOuvertureLogger } from "../../src/interfaces/moteur-ouverture-logger.interface";

export class LoggerSpy implements IMoteurOuvertureLogger {
    public erreurAppelee = false;
    public logs: Array<ILogOuverturePorteParams> = [];

    public logException(): void {
        this.erreurAppelee = true;
    }

    public logOuverturePorte(params: ILogOuverturePorteParams): void {
        this.logs.push(params);
    }

    public dernierLog(): ILogOuverturePorteParams {
        return this.logs[this.logs.length - 1];
    }
}
