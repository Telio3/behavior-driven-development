import { IPorte } from "../../src/interfaces/porte.interface";

export class PorteSpy implements IPorte {
    public ouvertureAppelee = false;

    public ouvrir(): void {
        this.ouvertureAppelee = true;
    }
}
