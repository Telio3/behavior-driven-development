import { Badge } from "../../src/classes/badge";
import { Personne } from "../../src/classes/personne";
import { IPlageHoraire } from "../../src/interfaces/plage-horaire.interface";

export class BadgeBuilder {
    private _proprietaire: Personne | undefined;
    private _bloque: boolean = false;
    private _plagesHoraires: IPlageHoraire[] | undefined;

    public build(): Badge {
        const badge = new Badge(this._plagesHoraires);

        if (this._proprietaire) badge.attribuerA(this._proprietaire);
        if (this._bloque) badge.bloquer();

        return badge;
    }

    public avecProprietaire(proprietaire?: Personne): BadgeBuilder {
        if (proprietaire) this._proprietaire = proprietaire;
        else this._proprietaire = new Personne('John', 'Doe');

        return this;
    }

    public bloque(): BadgeBuilder {
        this._bloque = true;

        return this;
    }

    public avecPlagesHoraires(plagesHoraires: IPlageHoraire[]): BadgeBuilder {
        this._plagesHoraires = plagesHoraires;

        return this;
    }
}