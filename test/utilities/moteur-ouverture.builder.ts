import { Badge } from "../../src/classes/badge";
import { MoteurOuverture } from "../../src/classes/moteur-ouverture";
import { IPorte } from "../../src/interfaces/porte.interface";
import { IMoteurOuvertureLogger } from "../../src/interfaces/moteur-ouverture-logger.interface";
import { LecteurDummy } from "../dummies/lecteur.dummy";
import { LecteurFake } from "../fakes/lecteur.fake";

export class MoteurOuvertureBuilder {
    private _porte: IPorte | undefined;
    private _lecteur: LecteurFake | LecteurDummy | undefined;
    private _lecteurCasse: boolean = false;
    private _logger: IMoteurOuvertureLogger | undefined;
    private _badges: Badge[] = [];
    private _badgeDetecte: Badge | undefined;

    public build(): MoteurOuverture {
        if (!this._porte) throw new Error('Porte non définie');

        if (this._lecteurCasse) this._lecteur = new LecteurDummy(this._porte);
        else this._lecteur = new LecteurFake(this._porte);

        const moteurOuverture = new MoteurOuverture(this._lecteur, this._logger);

        this._badges.forEach((badge) => {
            moteurOuverture.enregistrerBadge(badge);
        });

        if (this._badgeDetecte) this._lecteur.simulerDetectionBadge(this._badgeDetecte);

        return moteurOuverture;
    }

    public avecPorte(door: IPorte): MoteurOuvertureBuilder {
        this._porte = door;

        return this;
    }

    public avecLogger(logger: IMoteurOuvertureLogger): MoteurOuvertureBuilder {
        this._logger = logger;

        return this;
    }

    public avecLecteurCasse(): MoteurOuvertureBuilder {
        this._lecteurCasse = true;

        return this;
    }

    public enregistrerBadges(...badges: Badge[]): MoteurOuvertureBuilder {
        this._badges.push(...badges);

        return this;
    }

    public simulerDetectionBadge(badge: Badge): MoteurOuvertureBuilder {
        this._badgeDetecte = badge;

        return this;
    }
}
